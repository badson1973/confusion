from schemas import Employee
import csv
import uuid


def make_token():
    return str(uuid.uuid4().hex)


def get_data():
    with open("data/staff.csv", 'r', encoding='utf-8') as f:
        staff = f.readlines()
    return staff


def insert(item: Employee):
    with open("data/staff.csv", 'a', newline='', encoding='utf-8') as f:
        writer = csv.writer(f, dialect='excel')
        writer.writerow([item.name,
                         item.surname,
                         item.patronymic,
                         item.login,
                         item.password,
                         item.salary,
                         item.u_token,
                         item.expiration_token,
                         item.up_date_salary])


def overwriting(staff: list):
    temp = []
    for emp in staff:
        temp.append(emp[:-1].split(","))
    with open("data/staff.csv", 'w', newline='', encoding='utf-8') as f:
        writer = csv.writer(f, dialect='excel')
        writer.writerows(temp)


def read_up_date_salary(token):
    staff = get_data()
    for emp in staff:
        emp = emp.split(',')
        if token == emp[6]:
            return emp[8][:-1]


def read_token(login: str, password: str):
    staff = get_data()
    for emp in staff:
        emp = emp.split(',')
        if login == emp[3] and password == emp[4]:
            return emp[6]

