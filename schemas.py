from pydantic import BaseModel


class Employee(BaseModel):
    name: str
    surname: str
    patronymic: str
    login: str
    password: str
    salary: float
    u_token: str
    expiration_token: str
    up_date_salary: str

