FROM python:3.10
RUN set -xe
RUN curl -sSL https://install.python-poetry.org | python3 - --git https://github.com/python-poetry/poetry.git@master
ENV PATH="/root/.local/bin:$PATH"
RUN mkdir back
COPY main.py pyproject.toml poetry.lock README.md schemas.py settings.py functions.py /back
COPY ./data/staff.csv /back/data/
WORKDIR back
RUN poetry config virtualenvs.create false
RUN poetry install
EXPOSE 8000
CMD uvicorn main:app --reload --host 0.0.0.0 --port 8000
