from fastapi import FastAPI, Body
from functions import get_data, insert, make_token, overwriting
import datetime
from schemas import Employee
from settings import *

app = FastAPI()


@app.get("/")
def hello_from_service():
    return {"message": "Сервис запущен !!!"}


@app.post("/employee")
def create_employee(item: Employee):
    item.u_token = make_token()
    item.expiration_token = str(datetime.datetime.now().date() + datetime.timedelta(days=EXPIRATION_TOKEN))
    item.up_date_salary = str(datetime.datetime.now().date() + datetime.timedelta(days=UPDATE_SALARY))
    insert(item)
    return {
        "name": item.name,
        "surname": item.surname,
        "patronymic": item.patronymic,
        "login": item.login,
        "password": item.password,
        "salary": item.salary,
        "u_token": item.u_token,
        "expiration_token": item.expiration_token,
        "up_date_salary": item.up_date_salary
    }


@app.get("/dismissal")
def delete_employee(name: str, surname: str, patronymic: str):
    staff = get_data()
    is_delete = False
    for index, emp in enumerate(staff):
        emp = emp.split(',')
        if name == emp[0] and surname == emp[1] and patronymic == emp[2]:
            staff.pop(index)
            is_delete = True
    if is_delete:
        overwriting(staff)
        return {"message": "Сотрудник удален"}
    else:
        return {"message": "Сотрудник с таким именем не существует"}


@app.post("/salary")
def get_salary(token: str = Body(...)):
    staff = get_data()
    for emp in staff:
        emp = emp.split(',')
        # В автотестах токен приходит в виде строки вида "token=jfhgdsjvkjghfghsfgsfghfs"
        # Придется обрезать
        if token.startswith("token="):
            token = token[6:]
        if token == emp[6]:
            if datetime.datetime.now().date() > datetime.datetime.strptime(emp[7], "%Y-%m-%d").date():
                return {"message": "Старый токен"}
            return {"salary": emp[5], "up_date_salary": emp[8][:-1]}
    return {"message": "Неверный токен"}


@app.get("/login/{l}/pass/{p}")
def login(l: str, p: str):
    staff = get_data()
    for emp in staff:
        emp = emp.split(',')
        if l == emp[3] and p == emp[4]:
            if datetime.datetime.now().date() > datetime.datetime.strptime(emp[7], "%Y-%m-%d").date():
                return {"message": "Токен недействителен"}
            else:
                return {"token": emp[6]}
    return {"message": "Неверный логин или пароль"}


@app.post("/employee_manual")
def create_employee_manual(item: Employee):
    insert(item)
    return {
        "name": item.name,
        "surname": item.surname,
        "patronymic": item.patronymic,
        "login": item.login,
        "password": item.password,
        "salary": item.salary,
        "u_token": item.u_token,
        "expiration_token": item.expiration_token,
        "up_date_salary": item.up_date_salary
    }

