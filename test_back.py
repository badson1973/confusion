from fastapi.testclient import TestClient
from main import app
import datetime
from functions import *
from settings import *

client = TestClient(app)


# План
# 1. Получить доступ к сервису - тестируем маршрут "/"
# 2. Создать пользователя "Трофим"- тестируем маршрут "/emploee"
# 3. Создать пользователя "АнтиТрофим" с невалидным токеном - тестируем маршрут "/emploee_manual"
# 4. Получить токен Трофима (валидный) - тестируем маршрут "/login/{l}/pass/{p}".
# 5. Получить токен Трофима (валидный), используя неверные логин и пароль - тестируем маршрут "/login/{l}/pass/{p}". Вместо токена должно быть получено сообщение "Неверный логин или пароль".
# 6. Получить токен АнтиТрофима (невалидный) - тестируем маршрут "/login/{l}/pass/{p}". Вместо токена должно быть сообщение "Токен недействителен".
# 7. Получить зарплату и дату ее истечения для пользователя Трофима - тестируем маршрут "/salary"
# 8. Получить зарплату и дату ее истечения для пользователя АнтиТрофима - тестируем маршрут "/salary". Вместо желаемых данных должно быть получено сообщение "Неверный токен"
# 9. Удаление пользователя Трофим - тестируем маршрут "/dismissal".
# 10 Удаление пользователя АнтиТрофим, используя неверные данные в запросе - тестируем маршрут "/dismissal". Вместо удаления должно быть получено собщение "Сотрудник с таким именем не существует".
# 11. Удаление пользователя АнтиТрофим - тестируем маршрут "/dismissal".
# По завершении тестов тестовые сотрудники Трофим и АнтиТрофим должы быть удалены из хранилища.


# 1
def test_hello():
    res = client.get("/")
    assert res.status_code == 200
    assert res.json() == {"message": "Сервис запущен !!!"}


# 2
def test_create_employee():
    true_emploee = {
        "name": "Трофим",
        "surname": "Иванов",
        "patronymic": "Иванович",
        "login": "troff",
        "password": "1973",
        "salary": 55000.26,
        "u_token": "string",
        "expiration_token": "string",
        "up_date_salary": "string"
    }
    res = client.post("/employee", json=true_emploee, headers={"content-type": "application/json"})
    assert res.status_code == 200
    res_data = res.json()
    assert res_data["name"] == "Трофим"
    assert res_data["surname"] == "Иванов"
    assert res_data["patronymic"] == "Иванович"
    assert res_data["login"] == "troff"
    assert res_data["password"] == "1973"
    assert res_data["salary"] == 55000.26
    # Вычисляем даты
    assert res_data["expiration_token"] == str(
        datetime.datetime.now().date() + datetime.timedelta(days=EXPIRATION_TOKEN))
    assert res_data["up_date_salary"] == str(datetime.datetime.now().date() + datetime.timedelta(days=UPDATE_SALARY))
    # Получаем значение токена из хранилища и сравниваем
    token = read_token("troff", "1973")
    assert res_data["u_token"] == token


# 3
def test_create_employee_manual():
    fake_emploee = {
        "name": "АнтиТрофим",
        "surname": "Иванов",
        "patronymic": "Иванович",
        "login": "atroff",
        "password": "3791",
        "salary": 5500.26,
        "u_token": "jfhgdsjvkj",
        "expiration_token": "2022-06-30",
        "up_date_salary": "2022-09-18"
    }
    res = client.post("/employee_manual", json=fake_emploee, headers={"content-type": "application/json"})
    assert res.status_code == 200
    res_data = res.json()
    for k, v in fake_emploee.items():
        assert res_data[k] == v


# 4
def test_login():
    l = "troff"
    p = "1973"
    res = client.get(f'/login/{l}/pass/{p}')
    assert res.status_code == 200
    res_data = res.json()
    # Получаем значение токена из хранилища и сравниваем
    token = read_token(l, p)
    assert res_data["token"] == token


# 5
def test_login2():
    l = "troffim"
    p = "19973"
    res = client.get(f'/login/{l}/pass/{p}')
    assert res.status_code == 200
    res_data = res.json()
    assert res_data["message"] == "Неверный логин или пароль"


# 6
def test_login3():
    l = "atroff"
    p = "3791"
    res = client.get(f'/login/{l}/pass/{p}')
    assert res.status_code == 200
    res_data = res.json()
    assert res_data["message"] == "Токен недействителен"


# 7
def test_salary():
    # Получаем токен зная логин и пароль
    l = "troff"
    p = "1973"
    res = client.get(f'/login/{l}/pass/{p}')
    assert res.status_code == 200
    token = res.json()["token"]
    print(token)
    # Применяем токен
    res = client.post("/salary", data={"token": token})
    assert res.status_code == 200
    res_data = res.json()
    assert res_data["salary"] == '55000.26'
    # Если используется маршрут "/emploee", то даты вычисляются "на лету" в момент создания сотрудника.
    # Поэтому даты для сравнения читаем из хранилища
    up_date_salary = read_up_date_salary(token)
    assert res_data["up_date_salary"] == up_date_salary


# 8
def test_salary2():
    # Фейковый токен -
    token = "jfhgdsjvkjsfgsfghfs91683aa457ab477cbf538791a6dfcacd"
    res = client.post("/salary", data={"token": token})
    assert res.status_code == 200
    res_data = res.json()
    assert res_data["message"] == "Неверный токен"


# 9
def test_salary3():
    # Токен уже известен
    token = "jfhgdsjvkj"
    res = client.post("/salary", data={"token": token})
    assert res.status_code == 200
    res_data = res.json()
    assert res_data["message"] == "Старый токен"


# 10
def test_delete_employee():
    query = "/dismissal?name=Трофим&surname=Иванов&patronymic=Иванович"
    res = client.get(query)
    assert res.status_code == 200
    res_data = res.json()
    assert res_data["message"] == "Сотрудник удален"


# 11
def test_delete_employee2():
    query = "/dismissal?name=АнтиТрофим&surname=Сидор&patronymic=Маркович"
    res = client.get(query)
    assert res.status_code == 200
    res_data = res.json()
    assert res_data["message"] == "Сотрудник с таким именем не существует"


# 12
def test_delete_employee3():
    query = "/dismissal?name=АнтиТрофим&surname=Иванов&patronymic=Иванович"
    res = client.get(query)
    assert res.status_code == 200
    res_data = res.json()
    assert res_data["message"] == "Сотрудник удален"

